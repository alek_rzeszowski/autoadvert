
var price = "$1499/Mo.";
var make = "2015 Porsche 911 GT";
var imageNumber = 3;
var clientNumber = 1;
var transitionDelay = 2;  //how long each slide is up before it transitions
var finalSlideTime = 5;   //how long the last slide is on for
var transitionSpeed = 1.2;  //the speed of the fades

var copy = [];
	copy[1] = make,
	copy[2] = "I'm looking for a new owner.",
	copy[3] = "Take me home.";
	copy[4] = make;
var carText;
var currentImage = 1;

var finalSlideColor = 0xcf081a;

var action;
var adWidth = 720,
	adHeight = 300;

/** ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█ RENDERER █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

var renderer = PIXI.autoDetectRenderer(
  adWidth, adHeight,
  {antialias: true, transparent: false, resolution: 1, backgroundColor: finalSlideColor}
);



var elem = document.getElementById("canvas");
elem.appendChild(renderer.view);


var stage = new PIXI.Container();

/** ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█ LOADER █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

var images = []



//load client images into memory
PIXI.loader
	.add([
		"img/client001/01.jpg",
		"img/client001/02.jpg",
		"img/client001/03.jpg",
		"img/clicktext.png",
		"img/clicktext_down.png",
		"img/client001/logo.png"])
	.load(setup);



/** ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█ BUTTON FUNCTIONS █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/


function click(){
	// http://www.autooneleasing.ca/
	window.location.href = 'http://www.autooneleasing.ca/';
}

function downButton(){
	this.texture = PIXI.loader.resources["img/clicktext_down.png"].texture;
	renderer.render(stage);

}

function upButton(){
	this.texture = PIXI.loader.resources["img/clicktext.png"].texture;
	renderer.render(stage);
}



/** ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█ SETUP █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

function setup(){

/** ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█ CAR IMAGES █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/
	//load textures into sprites
	for (var i = imageNumber; i > 0; i--) {
		images[i] = new PIXI.Sprite(
			PIXI.loader.resources["img/client001/0" + i + ".jpg"].texture
		);
	};
		//determine their size first
		for (var x = imageNumber;x > 0;x --){
			var ratio = images[x].width/images[x].height; 
			images[x].width = adWidth;
			images[x].height = images[x].width/ratio;
			images[x].y = -1* (adHeight/2);

			stage.addChild(images[x]);                //add images to the stack

		}


	createText();



	introduceTextElements();

	renderer.render(stage);
}


function updateHandler(){
	renderer.render(stage);
}



function completeHandler(){
	// console.log('Richard Dunn: ' + currentImage);
	currentImage += 1;
	
	if (currentImage <= imageNumber) {
		fadeinText(0);
	}

	renderer.render(stage);

	introduceTextElements();

}



function introduceTextElements(){
	switch(currentImage){
		case 1:
				placeText('down');
			break;
		case 2:
				placeText('down');
			break;
		case 3:
				placeText('up');
			break;
		case 4:
				placeFinal();
			break;
	}
}




function fadeOutText(d){
	TweenLite.to(carText, 0.2, {alpha: "0", delay:d, onUpdate:updateHandler, onComplete: textFadeOutComplete});
	// console.log('fade out');
}

function fadeinText(d){
	TweenLite.to(carText, 0.2, {alpha: "1", delay:d, onUpdate:updateHandler, onComplete: textFadeInComplete});
	// console.log('fade in');

}


function transition(){
	TweenLite.to(images[currentImage], transitionSpeed, {alpha: "0", delay:0, onUpdate:updateHandler, onComplete:completeHandler});
}


function placeText(location){

	carText.text = copy[currentImage];

	carText.x = adWidth - carText.width - 20;

	if (location == "up"){
		carText.y = 10;
	} else if (location == "down"){
		carText.y = adHeight - carText.height - 10;
	}

}

function textFadeOutComplete(){
		if (currentImage <= imageNumber ){
		transition();
	} 
	// console.log('fade out COMPLETE');
}

function textFadeInComplete(){
	fadeOutText(transitionDelay);
}

function createText(){ //the first time carText is create and made invisible.

		var textStyle = {
		fontFamily: 'Helvetica', //Aller is default but we'll use helvetica because everyone has it
		fontWeight: 'bold',
		fontSize: '35px',
		fill: '#ffffff'
	}

	carText = new PIXI.Text(copy[currentImage], textStyle);

	carText.x = adWidth - carText.width - 20;
	carText.y = 10;

	stage.addChild(carText);
	fadeOutText(transitionDelay);
}




function placeFinal(){

/** ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█ ACTION BUTTON █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
*/

		 action = new PIXI.Sprite(
		 	PIXI.loader.resources["img/clicktext.png"].texture
		 	);

		 	action.anchor.x = 0.5;
		 	action.anchor.y = 0.5;
		 	action.x = adWidth/2;
		 	action.y = adHeight/2;

		 	action.alpha = 1;

			action.buttonMode = true;
			action.interactive = true;

			action
				.on('mousedown', click)
				.on('touchstart', click)

				.on('mouseover', downButton)
				.on('mouseout', upButton);


			stage.addChild(action);


				/** ░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█ LOGO █░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
		*/
			 var logo = new PIXI.Sprite(
			 	PIXI.loader.resources["img/client001/logo.png"].texture
			 	);

				var ratio = logo.width/logo.height;
				logo.width = 200;
				logo.height = logo.width/ratio;

				logo.x = adWidth - logo.width - 10;
				logo.y = adHeight - logo.height - 10; 	
				logo.alpha = 0.85;				

		stage.addChild(logo);


		var textStyle = {
			fontFamily: 'Aller',
			fontWeight: 'bold',
			fontSize: '35px',
			fill: '#ffffff'
		}

		var carName = new PIXI.Text(copy[4], textStyle);

		carName.anchor.x = 0.5;
		carName.anchor.y = 0.5;
		carName.x = adWidth/2;
		carName.y = 50;


		stage.addChild(carName);


		renderer.render(stage);


		TweenLite.to(carName, 1, {alpha: "0", delay: finalSlideTime, onUpdate:updateHandler});
		TweenLite.to(logo, 1, {alpha: "0", delay: finalSlideTime});
		TweenLite.to(action, 1, {alpha: "0", delay: finalSlideTime, onComplete: removeAll});

}

function removeAll(){
	// console.log("removing");
	for (var i = stage.children.length - 1; i >= 0; i--) {	
		stage.removeChild(stage.children[i]);
		// console.log(i);
	};

	currentImage = 1;
	// console.log(stage);

		setup();
}








